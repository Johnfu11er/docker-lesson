# DOCKER LESSON
This repository contains files and directions to provide a basic introduction to the configuration and use of `DOCKER`

### Setup
- Clone this git repository to your computer
  ```
  git clone git@gitlab.com:Johnfu11er/docker-lesson.git
  ```
- Navigate to the `docker-lesson` directory in the git clone location

### Verify Application Operation
- Install required node packages
  ```
  npm install
  ```
- Start the Tic-Tac-Toe application
  ```
  npm start
  ```
- If the above command does not automatically launch a web browser to the Tic-Tac-Toe application, open your web browser and navigate to:
  ```
  127.0.0.1:3000
  ```
- Close your application from the terminal with the `CTRL + C` command

### Build and Run the Tic-Tac-Toe application as a container
- Delete the `node_modules` directory in the `docker-lesson` directory
- Start the `Rancher Desktop` application
- Create a new file named `dockerfile` in the `docker-lesson` directory
  ```
  touch dockerfile
  ```
- Save the following code in the `dockerfile` file:
  ```
  FROM node:latest
  WORKDIR /usr/src/tic-tac-toe
  COPY package*.json ./
  
  RUN npm install
  COPY . .
  
  EXPOSE 3000
  
  CMD [ "npm", "start" ]
  ```
- From the `docker-lesson` directory, build the Tic-Tac-Toe application's docker container:
  ```
  docker build -t myapp-image .
  ```
- After the build finishes, run the command `docker image ls`, and you should see your `myapp-image` listed
- Run the docker container from the terminal:
  ```
  docker run -d -p 3000:3000 --name myapp myapp-image
  ```
- View the application from your web browser by navigating to:
  ```
  127.0.0.1:3000
  ```
- Stop the docker container:
  ```
  docker stop myapp
  ```
- Delete the container:
  ```
  docker rm myapp
  ```
- Delete any additional docker artifacts:
  ```
  docker system prune -a
  ```
  > Press "y" when prompted and press enter

### Run the Tic-Tac-Toe application container using docker-compose
- Create a new file named `docker-compose.yaml` in the `docker-lesson` directory
  ```
  touch docker-compose.yaml
  ```
- Save the following code in the `docker-compose.yaml` file
  ```
  version: "3.8"
  
  services:
    tic-tac-toe:
      image: myapp-image
      ports:
        - 3000:3000
  ```
- From the `docker-lesson` directory, build the Tic-Tac-Toe application's docker container:
  ```
  docker build -t myapp-image .
  ```
- Run the docker container from the terminal using `docker-compose`:
  ```
  docker-compose up -d
  ```
- View the application from your web browser by navigating to:
  ```
  127.0.0.1:3000
  ```
- Stop the docker container:
  ```
  docker-compose down
  ```
- Delete any additional docker artifacts:
  ```
  docker system prune -a
  ```